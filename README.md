<h1>Теперь этот репозиторий служит как тестовый. Мы приняли решение <a href='https://codeberg.org/Daemon-RE/daemon-re'>слиять</a> оба репозитория. </h1>
<h1 align="center">
  <img src="https://codeberg.org/Daemon-RE/daemonre-core/media/branch/main/assets/logo.jpg" alt="Daemon RE"><br>
</h1>
<p align="center">Daemon RE Modules Pack — это пак стандартных модулей, которые пишут разработчики бота.</p>